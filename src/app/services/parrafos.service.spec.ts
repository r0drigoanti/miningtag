import { TestBed } from '@angular/core/testing';

import { ParrafosService } from './parrafos.service';

describe('ParrafosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParrafosService = TestBed.get(ParrafosService);
    expect(service).toBeTruthy();
  });
});
