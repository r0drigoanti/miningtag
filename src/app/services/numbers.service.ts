import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NumbersService {



  constructor(private http: HttpClient) {

   }

  readonly api_url = 'http://168.232.165.184/prueba/array';

  getNumbers() {
    return this.http.get(this.api_url);
  }


}
