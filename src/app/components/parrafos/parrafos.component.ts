import { Component, OnInit } from '@angular/core';
import { ParrafosService } from '../../services/parrafos.service';
import { IRemoteResponse } from '../../models/number'


@Component({
  selector: 'app-parrafos',
  templateUrl: './parrafos.component.html',
  styleUrls: ['./parrafos.component.css']
})
export class ParrafosComponent implements OnInit {

  constructor(private ParrafosService: ParrafosService) { }

  ngOnInit() {
  }

  Parrafos : IRemoteResponse;
  parrafos_ev : Array<{indice:number; parrafo: string; number:number; numero_acumulativo:number; posicion:number}> = [];
  estado:any;

  getParrafos(){
    
    this.ParrafosService.getParrafos()  
    .subscribe(data => {
      var posicion = 0;
      if(data["success"]==true)
      {
          this.estado="true";
          this.Parrafos = data["data"];
          var indice = 0;
          var acumulativo = 0;

          for (var i = 0; i < Object.keys(this.Parrafos).length; i++)
          {
            indice++;
            if(i!=0)
            {
              acumulativo =  this.Parrafos[i].number + this.Parrafos[i-1].number;
            }
            else
            {
              acumulativo =  this.Parrafos[i].number;
            }

            if(indice == 1)
            {
              posicion = 1;
            }
            else
            {
              posicion = 0;
            }
            this.parrafos_ev.push({ "indice":indice, "parrafo": this.Parrafos[i].paragraph, "number": this.Parrafos[i].number, "numero_acumulativo": acumulativo, "posicion":posicion});
          }       
          var largo_evaluados = this.parrafos_ev.length;
          this.parrafos_ev[largo_evaluados-1].posicion = 9;

          this.ordenarParrafos();

          console.log(this.parrafos_ev);
      }
      else
      {
        this.estado="false";
      }
    });
  }

  ordenarParrafos(){

    var largo = this.parrafos_ev.length;
    
    for (var x = 0; x < largo; x++) {
        for (var i = 0; i < largo-x-1 ; i++) {
            if(this.parrafos_ev[i]["parrafo"].charAt(0) > this.parrafos_ev[i+1]["parrafo"].charAt(0)){
                var tmp = this.parrafos_ev[i+1]["parrafo"];
                this.parrafos_ev[i+1]["parrafo"] = this.parrafos_ev[i]["parrafo"];
                this.parrafos_ev[i]["parrafo"] = tmp;
            }
        }
    }

    
  }

}
